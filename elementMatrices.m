% Elementary matrices
% -------------------
%
% Create and export element matrices ke and me for plate element

clear
clc

syms H1 H2 a b t D rho1 rho2 E1 E2 nu

rhoEq = (rho1*H1 + rho2*H2)/t;

[M_N, K_N] = interpFunc(a,b,nu);

ke = a/2*b/2*simplify(gaussIntSymb(K_N, 3));
me = a/2*b/2*simplify(gaussIntSymb(M_N, 3));

ke = simplifyFraction(ke);
me = simplifyFraction(me);

% ke = D*ke;
% me = rhoEq*t*me;

% Extract common terms so there are no fractions inside the matrix
ke = ke*15*a^3*b^3;
me = me*7200/(a*b);

% Export to latex
matrix2latex(ke, 'ke.tex','sym', 1)
matrix2latex(me, 'me.tex', 'sym', 1)