% Computation of mode shapes of plate using FE
% --------------------------------------------

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

Na = 30;     % number of nodes in x direction
Nb = 30;     % number of nodes in y direction
dof = 3;     % number of dof/node
modeN = 10;  % number of modes to extract

% Plate size
at = 1;
bt = 1;

% t = 0.01; % thin plate, shear doesn't affect
% t = 0.05; % medium plate
t = 0.1; % thick plate, shear effect

% Material and section data
[E, nu, rho] = data('steel');
% [E, ~, ~, nu, ~, ~, ~, rho, ~, ~] = data('layers','base',t); % base
% [~, E, ~, nu, ~, ~, ~, ~, rho, ~] = data('ansys',t); % viscoelastic

D = E*t^3/(12*(1-nu^2));

% Element size
a = at/(Na-1);
b = bt/(Nb-1); 

plotModel(Na, Nb, at, bt)

% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(a,b,nu);


% Gaussian quadrature
% -------------------

% a/2 and b/2 to adjust interval from [-1 1; -1 1] to [0 a, 0 b] 

ke = D*a/2*b/2*gaussIntegrate(K_N, 3);
me = rho*t*a/2*b/2*gaussIntegrate(M_N, 3);

% Connectivity
% -------------
% The rows represent the nodes the element connects

% 13 --- 14 --- 15 --- 16
% |       |      |      | 
% |       |      |      |
% 9  --- 10 --- 11 --- 12
% |       |      |      | 
% |       |      |      |
% 5  ---  6 ---  7 ---  8
% |       |      |      | 
% |       |      |      |
% 1  ---  2 ---  3 ---  4

% Connectivity rows should start by nodes [1 2 3 5 6 7 9 10 11]
% the last number = n� of elements + n� of multiples of Na = 
% = (Na-1)*(Nb-1) + (Nb-1)
% that makes 1:12. The multiples of Na have to be removed to retain only 
% the needed nodes

Ne = (Na-1)*(Nb-1); % number of elements
nodes = 1:(Ne+(Nb-1));
nodes = nodes(mod(nodes,Na)~=0)';

connect = [nodes nodes+1 nodes+Na+1 nodes+Na];

K = assemble(ke, connect, dof*Na*Nb);
M = assemble(me, connect, dof*Na*Nb);

% Boundary conditions
% --------------------

% Case 1: simply supported in all edges
% Case 2: clamped in all edges
% Case 3: free
% Case 4: clampled in one edge
% Case 5: simply supported in all corners

boundary = 1;

% Boundaries
low   = 1:Na;
up    = (Nb*Na-Na+1):Nb*Na;
right = 1:Na:(Nb*Na-Na+1);
left  = Na:Na:Nb*Na;

% Corners
lowLeft  = intersect(low,left);
lowRight = intersect(low,right);
upLeft   = intersect(up,left);
upRight  = intersect(up,right);

switch boundary
    
    case 1
    % Simply supported in all ends
    border = [low up right left];
    border = unique(border);
    
    node = border';
    dof = ones(length(border),1);
    value = zeros(length(border),1);
    
    BC = [node dof value];
    
    case 2
    % Clamped in all ends
    border = [low up right left];
    border = unique(border);
    
    node = reshape(repmat(border,3,1), [length(border)*3 1]);
    dof = reshape(repmat([1 2 3],1,length(border)), [length(border)*3 1]);
    value = zeros(length(border)*3,1);
    
    BC = [node dof value];
    
    case 3
    % Free
    BC = [];
    
    case 4
    % Clamped in one end
    border = low;
    
    node = reshape(repmat(border,3,1), [length(border)*3 1]);
    dof = reshape(repmat([1 2 3],1,length(border)), [length(border)*3 1]);
    value = zeros(length(border)*3,1);
    
    BC = [node dof value];
    
    case 5
    % Simply supported in all corners
    border = [lowLeft lowRight upRight upLeft];
    
    node = border';
    dof = ones(length(border),1);
    value = zeros(length(border),1);
    
    BC = [node dof value];
  
    
end

M_BC = applyBC(M, BC);
K_BC = applyBC(K, BC);

% Eigenpairs
% ----------

[phi, lambda] = eigs(K_BC,M_BC,modeN,'smallestabs');

lambda = sqrt(diag(lambda));

% Plot
% ----

u = assembleBC(phi, BC);
u = u(1:3:end,:); % each mode is a column

% Choose mode to plot
mode = 6;

U = reshape(u(:,mode),Na,Nb);

% Coordinates of nodes
x = 0:a:at; 
y = 0:b:bt;

[X,Y] = meshgrid(x,y);

figure
fig = surf(X,Y,U');
title(['Mode number ' num2str(mode)])

% Computation of error
% ---------------------

omegaTheory = theoreticalFrec(at,bt,[D rho nu t],boundary);

if ~isnan(omegaTheory)
    
    if modeN <= length(omegaTheory)
        error = (lambda - omegaTheory(1:modeN))./lambda*100;
    else
        error = (lambda(1:length(omegaTheory)) - omegaTheory)./lambda(1:length(omegaTheory))*100;

    end

    fprintf('Error in first natural frecuencies [%%] \n');
    fprintf('%.2f\n', error);

end

fprintf('Natural frequencies [rad/s]\n')
fprintf('%.2f\n',lambda)