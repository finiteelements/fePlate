% Compute response of plate for different thickness and BC

H2 = [0.001, 0.005, 0.010];
boundary = 1:5;

for i = 1:length(boundary)
    
    for j = 1:length(H2)

        batchCLDresponse(H2(j), boundary(i))
        
    end
end