% Check correctness of matrices
clear
addpath('../functions')

% Data 
syms a b rho t nu E

a = 1;
b = 1;
t = 1;
E = 1;
rho = 1;
nuV = 0.3;

D = E*t^3/(12*(1-nuV^2));

% Compute stiffness and mass matrices
[M_N, K_N] = interpFunc(a,b);

% Sustitution in matrix before integration to check correctness of
% interpolation functions
syms xi eta nu
m = rho*t*subs(M_N, [xi, eta], [1,0]);
k = D*simplify(subs(K_N, [xi, eta, nu], [0,1,nuV]));

% a/2 and b/2 to adjust interval from [-1 1; -1 1] to [0 a; 0 b] 
ke = simplify(D*a/2*b/2*gaussIntegrate(K_N, 3)); % with 3 integration points error = 0
ke = subs(ke, nu, nuV);

% Exact element stiffness matrix
kExacto = simplify(D*a/2*b/2*int(int(K_N,xi,-1,1), eta,-1, 1));
kExacto = subs(kExacto, nu, nuV);

% Error
errorK = simplify(ke-kExacto); 

% Numerically integrated mass matrix 
me = simplify(rho*t*a/2*b/2*gaussIntegrate(M_N, 4)); % with 4 integration points error = 0

% Exact element mass matrix
mExacto = rho*t*a/2*b/2*int(int(M_N,xi,-1,1), eta,-1, 1);

% Error
errorM =simplify(me-mExacto);

% Compute numerical value if the lenght is not symbolic
if isfloat(a)
   me = double(me);
   ke = double(ke);
end

% Theoretical stiffness matrix in O�ate 2 --> pg 251