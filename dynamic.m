% Computation of FRF in a point of a plate by FE
% ----------------------------------------------

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

Na = 30;    % number of nodes in x direction
Nb = 30;     % number of nodes in y direction
dof = 3;    % number of dof/node

N = dof*Na*Nb; % size of the problem

% Material and section data

% Plate size
at = 1;
bt = 1;

% t = 0.01; % thin plate, shear doesn't affect
% t = 0.05; % medium plate
t = 0.1; % thick plate, shear effect

% Material and section data
% [E, nu, rho] = data('steel');
[E, ~, ~, nu, ~, ~, ~, rho, ~, ~] = data('layers','base',t);

D = E*t^3/(12*(1-nu^2));

% Element size
a = at/(Na-1);
b = bt/(Nb-1); 

plotModel(Na, Nb, at, bt)

% Input data
% ----------

% Boundary conditions

boundary = input('Boundary conditions? \n1: simply supported in all edges \n2: clamped in all edges \n3: free \n4: clampled in one edge \n5: simply supported in all corners \n');

point = input('Point force? (true/false)\n');

% Position of the point load 

if point 
    nodeLoad = input('Position of the point load? \n');
    dofLoad  = input('Dof of the point load? \n');
end

% Response 

nodeResp = input('Position of the response? \n');
dofResp  = input('Dof of the response? \n');

% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(a,b,nu);


% Gaussian quadrature
% -------------------

% a/2 and b/2 to adjust interval from [-1 1; -1 1] to [0 a, 0 b] 

ke = D*a/2*b/2*gaussIntegrate(K_N, 3);
me = rho*t*a/2*b/2*gaussIntegrate(M_N, 3);

% Connectivity
% -------------
% The rows represent the nodes the element connects

% 13 --- 14 --- 15 --- 16
% |       |      |      | 
% |       |      |      |
% 9  --- 10 --- 11 --- 12
% |       |      |      | 
% |       |      |      |
% 5  ---  6 ---  7 ---  8
% |       |      |      | 
% |       |      |      |
% 1  ---  2 ---  3 ---  4

% Connectivity rows should start by nodes [1 2 3 5 6 7 9 10 11]
% the last number = n� of elements + n� of multiples of Na = 
% = (Na-1)*(Nb-1) + (Nb-1)
% that makes 1:12. The multiples of Na have to be removed to retain only 
% the needed nodes

Ne = (Na-1)*(Nb-1); % number of elements
nodes = 1:(Ne+(Nb-1));
nodes = nodes(mod(nodes,Na)~=0)';

connect = [nodes nodes+1 nodes+Na+1 nodes+Na];

K = assemble(ke, connect, dof*Na*Nb);
M = assemble(me, connect, dof*Na*Nb);

% Load
% -----

if point 

    % Point load
    P = 1;
    values = [nodeLoad dofLoad P]; % [node, dof, value]

    F = assemblePointF(values, N);

else
    
    nodeLoad = 0;
    % Distributed load in all elements
    values = [nodes ones(size(nodes)) ones(size(nodes))]; % [startNode, dof, value]

    F = assembleForce(a, b, values, connect, N);

end

% Boundary conditions
% --------------------

% Case 1: simply supported in all edges
% Case 2: clamped in all edges
% Case 3: free
% Case 4: clampled in one edge
% Case 5: simply supported in all corners

% Boundaries
low   = 1:Na;
up    = (Nb*Na-Na+1):Nb*Na;
right = 1:Na:(Nb*Na-Na+1);
left  = Na:Na:Nb*Na;

% Corners
lowLeft  = intersect(low,left);
lowRight = intersect(low,right);
upLeft   = intersect(up,left);
upRight  = intersect(up,right);

switch boundary
    
    case 1
    % Simply supported in all ends
    border = [low up right left];
    border = unique(border);
    
    node = border';
    dof = ones(length(border),1);
    value = zeros(length(border),1);
    
    BC = [node dof value];
    
    case 2
    % Clamped in all ends
    border = [low up right left];
    border = unique(border);
    
    node = reshape(repmat(border,3,1), [length(border)*3 1]);
    dof = reshape(repmat([1 2 3],1,length(border)), [length(border)*3 1]);
    value = zeros(length(border)*3,1);
    
    BC = [node dof value];
    
    case 3
    % Free
    BC = [];
    
    case 4
    % Clamped in one end
    border = low;
    
    node = reshape(repmat(border,3,1), [length(border)*3 1]);
    dof = reshape(repmat([1 2 3],1,length(border)), [length(border)*3 1]);
    value = zeros(length(border)*3,1);
    
    BC = [node dof value];
    
    case 5
    % Simply supported in all corners
    border = [lowLeft lowRight upRight upLeft];
    
    node = border';
    dof = ones(length(border),1);
    value = zeros(length(border),1);
    
    BC = [node dof value];
  
    
end

M_BC = applyBC(M, BC);
K_BC = applyBC(K, BC);
F_BC = applyBC(F,BC);


% Response
% --------

mapResp = (nodeResp-1)*3 + dofResp;

% Unit vector with 1 where the response is computed
e = sparse(mapResp,1,1,N,1);
e = applyBC(e, BC);

steps = 1000;
fMin  = 0;
fMax  = 20000;

resp = zeros(steps,1);

f = linspace(fMin, fMax, steps); % rad/s

x = zeros(steps,1);

% Load variation in frequency

% Impulse load
P = ones(steps,1);

% Compute response in point 

for t = 1:steps
   
    s = 1i*f(t);
    Z = M_BC*s^2 + K_BC;
    
    h = Z\e; % only one column of H needed
    
    x(t) = h'*F_BC*P(t);
    
end

figure(2)

subplot(211), semilogy(f,abs(x)) 
title({'FRF', ['Input node: ' num2str(nodeLoad)], [' Output node: ' num2str(nodeResp)]})
xlabel('\omega [rad/s]'), ylabel('Amplitude [m]')
hold on

subplot(212), plot(f,angle(x))
xlabel('\omega [rad/s]'), ylabel('Angle [rad]')
hold on

%%

% Response for every point
% ------------------------

I = eye(length(Z),'like',Z);

x = zeros(length(Z),steps);

for t = 1:steps
   
    s = 1i*f(t);
    Z = M_BC*s^2 + K_BC;
    
    H = Z\I;
    
    x(:,t) = H*F_BC*P(t);
    
end

% Assemble x
x = assembleBC(x, BC);

% RMS response of all points
x = x(1:3:end,:); % each column is the response at a certain f
rms = mean(x.^2).^0.5;

figure(3)
semilogy(f,rms)
title('RMS ')
xlabel('\omega [rad/s]'), ylabel('Amplitude [m]')
hold on

