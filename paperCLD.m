% Computation of mode shapes of plate using FE and equivalent D
% --------------------------------------------------------------
%
% The eigenpairs for a plate made by several materials having one of them
% fractional damping. An equivalent modulus derived from the one for beams
% is used.

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

Na = 50;    % number of nodes in x direction
Nb = 50;    % number of nodes in y direction
dof = 3;    % number of dof/node
modeN = 10; % number of modes to extract

% Plate size 
at = 0.1;
bt = 0.1;

% Data
H1 = 0.001;
% H2 = 0.001;
% H2 = 0.005;
H2 = 0.010;
H3 = H1;

t = H1 + H2 + H3;
rho1 = 7782; 
rho2 = 1423;
rho3 = rho1;

E1 = 176.2E9;
nu = 0.3;

% Fractional damping
E_0   = 0.353E9;
E_inf = 3.462E9;
tau   = 314.9E-6;
alpha = 0.873;

syms f

E2(f) = (E_0 + E_inf*(1i*tau*f)^alpha)/(1+(1i*tau*f)^alpha);

E3 = E1;

rhoEq = (rho1*H1 + rho2*H2+ rho3*H3)/t;

G1 = E1/(2*(1+nu));
G2 = E2/(2*(1+nu)); 
G3 = E3/(2*(1+nu));

syms x f

% Neutral plane
hn = (E1*H1^2 + E2*H2*(2*H1+H2) + E3*H3*(2*H1+2*H2+H3))/(2*(E1*H1+E2*H2+E3*H3));

lim1 = -hn;
lim2 = H1 - hn;
lim3 = H1 + H2 - hn;
lim4 = H1 + H2 + H3 - hn; 

I1 = int(x^2, lim1, lim2);
I2 = int(x^2, lim2, lim3);
I3 = int(x^2, lim3, lim4); 

A = E1/(1-nu^2);
B = E2/(1-nu^2);
C = E3/(1-nu^2);

Deq = simplify(A*I1 + B*I2 + C*I3);
Beq = simplify(E1*I1 + E2*I2 + E3*I3);

omega1 = int(E1*x,x,lim1);
K1 = (G1*Beq^2)/int(omega1^2, lim1, lim2);
K1 = simplify(K1); 
    
omega2 = E1*H1*(hn-H1/2)+ E2*((hn-H1)^2 - x^2)/2;

K2 = (G2*Beq^2)/int(omega2^2, x, lim2, lim3);
K2 = simplify(K2);
omega3 = int(E3*x,x,lim4);
K3 = (G3*Beq^2)/int(omega3^2, x, lim3, lim4);
K3 = simplify(K3);
Keq(f) = 1/(1/K1 + 1/K2 + 1/K3);

phi(f) = f*sqrt(rhoEq*t*Deq)/(2*Keq); % B --> D

% D_eq takes into account different material, D(f) shear

D(f) = Deq/(sqrt(1+phi(f)^2)+phi(f))^2;

% Evolution of D with frequency
% -----------------------------
figure
ff = 0:30:10000*2*pi;

% Equivalent flexural stiffness RKU
D1 = E1*H1^3/(12*(1-nu));
D3 = E3*H3^3/(12*(1-nu));
H31 = (H1+H3)/2 + H2;
S = 1/(E1*H1) + 1/(E3*H3);
Y = 12*H31^2/(S*(E1*H1^3 + E3*H3^3));
k = sqrt(ff).*((rhoEq*(H1+H2+H3))./double(Deq(ff))).^0.25; % Bending wave number
X = double(G2(ff))*S./(double(k).^2*H2);
D_RKU = (D1+D3)*(1+X.*Y./(1+X));

subplot(211), plot(ff,real(double(D(ff))), ff, real(D_RKU))
xlabel('\omega (rad/s)'), ylabel('Re(D)'), legend('New model', 'RKU')
subplot(212), plot(ff,imag(double(D(ff))), ff, imag(D_RKU))
xlabel('\omega (rad/s)'), ylabel('Im(D)')

% Element size
a = at/(Na-1);
b = bt/(Nb-1); 

plotModel(Na, Nb, at, bt)

% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(a,b,nu);


% Gaussian quadrature
% -------------------

% a/2 and b/2 to adjust interval from [-1 1; -1 1] to [0 a, 0 b] 

ke = double(D(0))*a/2*b/2*gaussIntegrate(K_N, 3);
me = rhoEq*t*a/2*b/2*gaussIntegrate(M_N, 3);

% Connectivity
% -------------
% The rows represent the nodes the element connects

% 13 --- 14 --- 15 --- 16
% |       |      |      | 
% |       |      |      |
% 9  --- 10 --- 11 --- 12
% |       |      |      | 
% |       |      |      |
% 5  ---  6 ---  7 ---  8
% |       |      |      | 
% |       |      |      |
% 1  ---  2 ---  3 ---  4

% Connectivity rows should start by nodes [1 2 3 5 6 7 9 10 11]
% the last number = n� of elements + n� of multiples of Na = 
% = (Na-1)*(Nb-1) + (Nb-1)
% that makes 1:12. The multiples of Na have to be removed to retain only 
% the needed nodes

Ne = (Na-1)*(Nb-1); % number of elements
nodes = 1:(Ne+(Nb-1));
nodes = nodes(mod(nodes,Na)~=0)';

connect = [nodes nodes+1 nodes+Na+1 nodes+Na];

K = assemble(ke, connect, dof*Na*Nb);
M = assemble(me, connect, dof*Na*Nb);

% Boundary conditions
% --------------------

% Case 1: simply supported in all edges
% Case 2: clamped in all edges
% Case 3: free
% Case 4: clampled in one edge
% Case 5: simply supported in all corners

boundary = 2;

% Boundaries
low   = 1:Na;
up    = (Nb*Na-Na+1):Nb*Na;
right = 1:Na:(Nb*Na-Na+1);
left  = Na:Na:Nb*Na;

% Corners
lowLeft  = intersect(low,left);
lowRight = intersect(low,right);
upLeft   = intersect(up,left);
upRight  = intersect(up,right);

switch boundary
    
    case 1
    % Simply supported in all ends
    border = [low up right left];
    border = unique(border);
    
    node = border';
    dof = ones(length(border),1);
    value = zeros(length(border),1);
    
    BC = [node dof value];
    
    case 2
    % Clamped in all ends
    border = [low up right left];
    border = unique(border);
    
    node = reshape(repmat(border,3,1), [length(border)*3 1]);
    dof = reshape(repmat([1 2 3],1,length(border)), [length(border)*3 1]);
    value = zeros(length(border)*3,1);
    
    BC = [node dof value];
    
    case 3
    % Free
    BC = [];
    
    case 4
    % Clamped in one end
    border = low;
    
    node = reshape(repmat(border,3,1), [length(border)*3 1]);
    dof = reshape(repmat([1 2 3],1,length(border)), [length(border)*3 1]);
    value = zeros(length(border)*3,1);
    
    BC = [node dof value];
    
    case 5
    % Simply supported in all corners
    border = [lowLeft lowRight upRight upLeft];
    
    node = border';
    dof = ones(length(border),1);
    value = zeros(length(border),1);
    
    BC = [node dof value];
  
    
end

M_BC = applyBC(M, BC);
K_BC = applyBC(K, BC);

% Eigenpairs
% ----------

% If free, compute more eigenpairs
if boundary == 3    
    modeN = modeN + 3;
end

% Original eigenvalues
[mode, lambda] = eigs(K_BC,M_BC,modeN,'smallestabs');

omega = real(sqrt(diag(lambda)));

fprintf('Natural frequencies not taking into account shear:\n')
fprintf('%.2f\n',omega)

% Iterative method
tol = 0.1;
[modeNew, lambdaNew] = iterativeEigs(K_BC, M_BC, D, modeN, tol);
omegaNew = real(sqrt(lambdaNew));

fprintf('\nNatural frequencies taking shear and damping into account:\n')
fprintf('%.2f\n', omegaNew)

% Loss factor
% 
% lambda = omega^2*(1 + i*eta)

eta = imag(diag(lambda))./real(diag(lambda));
etaNew = imag(lambdaNew)./real(lambdaNew);

fprintf('\nDamping ratio taking shear and damping into account:\n')
fprintf('%.4f\n',etaNew)

% Modal strain energy
% NOTE: same values as loss factor 
strainEn = zeros(modeN,1);

for i = 1:modeN
    strainEn(i) = (modeNew(:,i)'*imag(K_BC*double(D(omegaNew(i))/D(0)))*modeNew(:,i))/(modeNew(:,i)'*real(K_BC*double(D(omegaNew(i))/D(0)))*modeNew(:,i)); 
end

fprintf('\nModal strain energy:\n')
fprintf('%.4f\n',strainEn)

% RKU
[modeRKU, lambdaRKU] = rku(K_BC, M_BC, E1, E2, E3, nu, rhoEq, double(D(0)), [H1 H2 H3], modeN, tol);
omegaRKU = real(sqrt(lambdaRKU));

fprintf('\nNatural frequencies (RKU):\n')
fprintf('%.2f\n',omegaRKU)

% Loss factor
etaRKU = imag(lambdaRKU)./real(lambdaRKU);
fprintf('\nDamping ratio for RKU:\n')
fprintf('%.4f\n',etaRKU)

% Strain energy
% NOTE: same values as loss factor
[~, strainEnRKU] = rkuSE(K_BC, M_BC, E1, E2, E3, nu, rhoEq, double(D(0)), [H1 H2 H3], modeN, tol);

fprintf('\nModal strain energy (RKU):\n')
fprintf('%.4f\n',strainEnRKU)

% Modes
% -----

u = assembleBC(mode, BC);
u = u(1:3:end,:); % each mode is a column

uNew = assembleBC(modeNew, BC);
uNew = uNew(1:3:end,:); % each mode is a column

uRKU = assembleBC(modeRKU, BC);
uRKU = uRKU(1:3:end,:); % each mode is a column

% Plot
% ----

% Choose mode to plot
modeNumber = 2;

U = reshape(u(:,modeNumber),Na,Nb)';
URKU = reshape(u(:,modeNumber),Na,Nb)';
UNew = reshape(uNew(:,modeNumber),Na,Nb)';

% Coordinates of nodes
x = 0:a:at; 
y = 0:b:bt;

% Animate RKU 
animateMode(URKU, {x,y}), title(['Mode number ' num2str(modeNumber) '. RKU'])

% Animate proposed mode
% animateMode(UNew, {x,y}, ['modes/CLD' num2str(H2*1000) 'mmB' num2str(boundary) '.gif']), title(['Mode number ' num2str(modeNumber) '. Proposed'])

% Plot natural frequencies with and without shear and damping
figure
fig = plot(1:modeN, omega,'*', 1:modeN, omegaRKU,'o', 1:modeN, omegaNew,'+', 'Linewidth', 1.5);
legend('No shear', 'RKU', 'Damping and shear')

title('Natural frequencies of plate')
xlabel('Mode')
ylabel('Frequency [rad/s]')

figure

fig = plot(1:modeN, eta,'*', 1:modeN, etaRKU,'o', 1:modeN, etaNew,'+','Linewidth', 1.5);
legend('No shear', 'RKU', 'With shear')

title('Damping of the plate')
xlabel('Mode')
ylabel('\eta')

% Export modes for MAC
% ---------------------
% Coordinates of points
[X,Y] = meshgrid(x,y);
x = reshape(X',[size(X,1)*size(X,2), 1]);
y = reshape(Y',[size(Y,1)*size(Y,2), 1]);

save(['modes/modesCLD' num2str(H2*1000) 'mmB' num2str(boundary)],'x','y','omegaNew','omegaRKU','uNew','uRKU')