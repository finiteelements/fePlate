% Computation of mode shapes of plate using FE and equivalent D
% --------------------------------------------------------------
%
% The eigenpairs for a plate made by several materials having one of them
% structural damping. An equivalent modulus derived from the one for beams
% is used.

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

Na = 50;     % number of nodes in x direction
Nb = 50;     % number of nodes in y direction

dof = 3;     % number of dof/node
modeN = 10;  % number of modes to extract

N = dof*Na*Nb; % size of the problem   

% Plate size 
at = 0.1;    % when a x b = 1x1 behaves like a thin plate
bt = 0.1;

% Data
H1 = 0.002;
% H2 = 0.002;  % when H2 low, Oberst and new model nearly identical
% H2 = 0.006;
H2 = 0.010;
H3 = 0;

t = H1 + H2 + H3;

rho1 = 7782; 
rho2 = 1423;
rho3 = rho1;

E1 = 176.2E9;
nu = 0.3;

% Complex modulus
E2 = 3.462E9;
eta = 0.8;

E2 = E2*(1+1i*eta); 

E3 = 0;

rhoEq = (rho1*H1 + rho2*H2+ rho3*H3)/t;

G1 = E1/(2*(1+nu));
G2 = E2/(2*(1+nu)); 
G3 = E3/(2*(1+nu));

syms x f

% Neutral plane
hn = (E1*H1^2 + E2*H2*(2*H1+H2) + E3*H3*(2*H1+2*H2+H3))/(2*(E1*H1+E2*H2+E3*H3));

lim1 = -hn;
lim2 = H1 - hn;
lim3 = H1 + H2 - hn;
lim4 = H1 + H2 + H3 - hn; 

I1 = int(x^2, lim1, lim2);
I2 = int(x^2, lim2, lim3);
I3 = int(x^2, lim3, lim4); 

A = E1/(1-nu^2);
B = E2/(1-nu^2);
C = E3/(1-nu^2);

Deq = simplify(A*I1 + B*I2 + C*I3);
Beq = simplify(E1*I1 + E2*I2 + E3*I3);

omega1 = int(E1*x,x,lim1);
K1 = (G1*Beq^2)/int(omega1^2, lim1, lim2);
K1 = simplify(K1);

omega2 = int(x,x,lim3);
K2 = (G2*Beq^2)/(E2^2*int(omega2^2, x, lim2, lim3)); % E2 outside int for faster computation
Keq(f) = 1/(1/K1 + 1/K2);

% S --> t (quadratic shear) TODO: linear shear 4/3

phi(f) = f*sqrt(rhoEq*t*Deq)/(2*Keq); % B --> D

% D_eq takes into account different material, D(f) shear

D(f) = Deq/(sqrt(1+phi(f)^2)+phi(f))^2;

% Evolution of D with frequency
figure
ff = 0:30:10000*2*pi;
subplot(211), plot(ff, real(double(D(ff))),ff,real(double(Deq))*ones(size(ff)))
xlabel('\omega[rad/s]'), ylabel('Re(D)'), legend('New model','Oberst')
subplot(212), plot(ff, imag(double(D(ff))),ff,imag(double(Deq))*ones(size(ff)))
xlabel('\omega[rad/s]'), ylabel('Im(D)')

% Element size
a = at/(Na-1);
b = bt/(Nb-1); 

plotModel(Na, Nb, at, bt)

% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(a,b,nu);


% Gaussian quadrature
% -------------------

% a/2 and b/2 to adjust interval from [-1 1; -1 1] to [0 a, 0 b] 

ke = double(D(0))*a/2*b/2*gaussIntegrate(K_N, 3);
me = rhoEq*t*a/2*b/2*gaussIntegrate(M_N, 3);

% Connectivity
% -------------
% The rows represent the nodes the element connects

% 13 --- 14 --- 15 --- 16
% |       |      |      | 
% |       |      |      |
% 9  --- 10 --- 11 --- 12
% |       |      |      | 
% |       |      |      |
% 5  ---  6 ---  7 ---  8
% |       |      |      | 
% |       |      |      |
% 1  ---  2 ---  3 ---  4

% Connectivity rows should start by nodes [1 2 3 5 6 7 9 10 11]
% the last number = n� of elements + n� of multiples of Na = 
% = (Na-1)*(Nb-1) + (Nb-1)
% that makes 1:12. The multiples of Na have to be removed to retain only 
% the needed nodes

Ne = (Na-1)*(Nb-1); % number of elements
nodes = 1:(Ne+(Nb-1));
nodes = nodes(mod(nodes,Na)~=0)';

connect = [nodes nodes+1 nodes+Na+1 nodes+Na];

K = assemble(ke, connect, N);
M = assemble(me, connect, N);

% Boundary conditions
% --------------------

% Case 1: simply supported in all edges
% Case 2: clamped in all edges
% Case 3: free
% Case 4: clampled in one edge
% Case 5: simply supported in all corners

boundary = 2;

% Boundaries
low   = 1:Na;
up    = (Nb*Na-Na+1):Nb*Na;
right = 1:Na:(Nb*Na-Na+1);
left  = Na:Na:Nb*Na;

% Corners
lowLeft  = intersect(low,left);
lowRight = intersect(low,right);
upLeft   = intersect(up,left);
upRight  = intersect(up,right);

switch boundary
    
    case 1
    % Simply supported in all ends
    border = [low up right left];
    border = unique(border);
    
    node = border';
    dof = ones(length(border),1);
    value = zeros(length(border),1);
    
    BC = [node dof value];
    
    case 2
    % Clamped in all ends
    border = [low up right left];
    border = unique(border);
    
    node = reshape(repmat(border,3,1), [length(border)*3 1]);
    dof = reshape(repmat([1 2 3],1,length(border)), [length(border)*3 1]);
    value = zeros(length(border)*3,1);
    
    BC = [node dof value];
    
    case 3
    % Free
    BC = [];
    
    case 4
    % Clamped in one end
    border = low;
    
    node = reshape(repmat(border,3,1), [length(border)*3 1]);
    dof = reshape(repmat([1 2 3],1,length(border)), [length(border)*3 1]);
    value = zeros(length(border)*3,1);
    
    BC = [node dof value];
    
    case 5
    % Simply supported in all corners
    border = [lowLeft lowRight upRight upLeft];
    
    node = border';
    dof = ones(length(border),1);
    value = zeros(length(border),1);
    
    BC = [node dof value];
  
end

M_BC = applyBC(M, BC);
K_BC = applyBC(K, BC);

% Eigenpairs
% ----------

% Oberst
[modeOb, lambdaOb] = eigs(K_BC,M_BC,modeN,'smallestabs');

omegaOb = real(sqrt(diag(lambdaOb)));

fprintf('Natural frequencies without shear but with damping (Oberst):\n')
fprintf('%.2f\n', omegaOb)


% Iterative method
tol = 0.1;
[modeNew, lambdaNew] = iterativeEigs(K_BC, M_BC, D, modeN, tol);
omegaNew = real(sqrt(lambdaNew));

% Use imaginary part as stopping criteria
% tol = 1;
% [modeNew, lambdaNew] = iterativeEigsEta(K_BC, M_BC, D, modeN, tol);
% omegaNew = real(sqrt(lambdaNew));

fprintf('\nNatural frequencies taking shear and damping into account:\n')
fprintf('%.2f\n', omegaNew)


% Plot
% ----

u = assembleBC(modeNew, BC);
u = u(1:3:end,:); % each mode is a column

% Choose mode to plot
modeNumber = 3;

U = reshape(u(:,modeNumber),Na,Nb);

% Coordinates of nodes
x = 0:a:at; 
y = 0:b:bt;

[X,Y] = meshgrid(x,y);

U = real(U);

figure
fig = surf(X,Y,U');
title(['Mode number ' num2str(modeNumber)])

% Plot natural frequencies with and without shear and damping
figure

fig = plot(1:modeN, omegaOb,'o', 1:modeN, omegaNew,'+', 'Linewidth', 1.5);
legend('Oberst', 'Damping and shear')

title('Natural frequencies of plate')
xlabel('Mode')
ylabel('Frequency [rad/s]')

% Effect of shear in damping
% 
% lambda = omega^2*(1 + i*eta)

figure

etaOb = imag(diag(lambdaOb))./real(diag(lambdaOb));
etaNew = imag(lambdaNew)./real(lambdaNew);
fig = plot(1:modeN, etaOb,'o', 1:modeN, etaNew,'+','Linewidth', 1.5);
legend('Oberst', 'With shear')

title('Damping of the plate')
xlabel('Mode')
ylabel('\eta')