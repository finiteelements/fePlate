% Compute modes of plate for different thickness and BC

H2 = [0.001, 0.005, 0.01];
boundary = 1:5;

for i = 1:length(H2)
    for j = 1:length(boundary)
        batchCLDmodes(H2(i), boundary(j))
    end
end