function B = applyBC(A, BC)
% Applies boundary conditions to matrix
%
% B = APPLYBC(A, BC)
%
% where:
% - A: matrix where BCs should be applyed
% - BC: [node, dof, value] triplets, values are used to create the
%       response
%
% See also ASSEMBLEBC

% If there are no BC conditions the matrix stays the same 
if isempty(BC)
    
    B = A;
    return
    
end

% Extract info
node = BC(:,1);
dof  = BC(:,2);

map  = (node-1)*3 + dof;
map  = sort(map, 'descend');

if size(A,2) > 1
    % For matrices
    A(:,map) = []; 
    A(map,:) = [];

else
    % For vector
    A(map) = [];
end

B = A;

end