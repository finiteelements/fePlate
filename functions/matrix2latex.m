function matrix2latex(matrix, filename, varargin)
% Exports matrix to LaTeX matrix for equations
%
% MATRIX2LATEX(matrix, filename, varargin)
% 
% where
%   - matrix is a 2 dimensional numerical, symbolic or cell array
%   - filename is a valid filename, in which the resulting latex code will
%   be stored
%   - varargs is one ore more of the following (denominator, value) combinations
%      + 'format', 'value' -> Can be used to format the input data. 'value'
%      has to be a valid format string, similar to the ones used in
%      fprintf('format', value);
%      + 'type', 'value' -> type of LaTeX matrix. Default is 'bmatrix' 
%      + 'sym', 0/1 or true/false -> if symmetric only plot lower triangular
%
% Example input:
%   matrix = [1.5 1.764; 3.523 0.2];
%   matrix2latex(matrix, 'out.tex','format', '%.2f');
%
% The resulting latex file can be included into any latex document by:
% \input{out.tex}. Remember to put \usepackage{amsmath} in your preamble
% and to change \setcounter{MaxMatrixCols}{N} N > matrix size for bmatrix
% to work if your matrix is bigger than 10 columns!
% 
% Ondiz Zarraga, 2019
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <https://www.gnu.org/licenses/>.
%
% Original matrix2latex by M. Koehler (koehler@in.tum.de), 2004 licensed
% under GNU GPL

format = [];
sym = [];
type = 'bmatrix'; % default is bmatrix

if (rem(nargin,2) == 1 || nargin < 2)
    error('Incorrect number of arguments to %s.', mfilename);
end
okargs = {'format', 'type', 'sym'};
for j=1:2:(nargin-2)
    pname = varargin{j};
    pval = varargin{j+1};
    k = find(strcmpi(pname, okargs));
    if isempty(k)
        error('Unknown parameter name: %s.', pname);
    elseif length(k)>1
        error('Ambiguous parameter name: %s.', pname);
    else        
        switch(k)
        case 1  % format
            format = lower(pval);
        case 2  % type of matrix
            type = pval;
        case 3 % symmetric
            sym = pval;
        end
    end
end
fid = fopen(filename, 'w');

if isnumeric(matrix)
    matrix = num2cell(matrix);       
    if(~isempty(format))
        matrix = cellfun(@(x) num2str(x, format), matrix, 'UniformOutput', 0);
    else
        matrix = cellfun(@num2str, matrix, 'UniformOutput', 0);
    end

else
    % Convert symbolic array to text array
    matrix = arrayfun(@char, matrix, 'UniformOutput', 0);

    greek = {'alpha','beta','gamma','delta','epsilon','zeta','eta',...
             'theta','iota','kappa','lamda','mu','nu','xi','omicron',...
             'pi','rho','sigma','tau','upsilon','phi','chi','psi','omega'};

    expGreek = '[a-z]{2,}';
    expFrac = '\((?<num>\S+)\)\/\((?<den>\S+)\)|\((?<num>\S+)\)\/(?<den>\d+)';

    for i=1:size(matrix,1)
        for j=1:size(matrix,2)
        % Add \ to greek letters    
        matchGreek = regexp(matrix{i,j},expGreek,'match');
        for k = 1:length(matchGreek)
            if ismember(matchGreek{k},greek)
            matrix{i,j}= strrep(matrix{i,j},matchGreek{k},['\' matchGreek{k} ' ']);
            end
        end

        % Change ()/() to \fraction{}{}     
        match = regexp(strrep(matrix{i,j},' ',''),expFrac,'names');
            if ~isempty(match)
                matrix{i,j}= ['\frac{' match.num '}{' match.den '}'];
            end
        end
    end

    % Delete multiplication sign (*)
    matrix = cellfun(@(x) strrep(x,'*',''), matrix, 'UniformOutput', 0);
end

fprintf(fid, ['\\begin{' type '}\n']);

rows = size(matrix, 1);
cols = size(matrix, 2);

% If symmetric just export lower triangular
if (~isempty(sym)) && sym
    for h=1:rows
        fprintf(fid, '%s', [strjoin(matrix(h,1:h),'&') repmat('&',1,cols-h)]);
        fprintf(fid, '\\\\\n');
    end
else

    for h=1:rows
        fprintf(fid, '%s', strjoin(matrix(h,:),'&'));
        fprintf(fid, '\\\\\n');
    end

end

fprintf(fid, ['\\end{' type '}\n']);

fclose(fid);