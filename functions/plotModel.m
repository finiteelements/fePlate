function plotModel(Na, Nb, at, bt)

% Plot model
%
% PLOTMODEL(Na, Nb, at, bt)
%
% Na: number of nodes in x direction
% Nb: number of nodes in y direction
% at: total length in x direction
% bt: total length in y direction

% Length of elements
a = at/(Na-1);
b = bt/(Nb-1); 

% Coordinates of nodes
x = 0:a:at; 
y = 0:b:bt;

[X,Y] = meshgrid(x,y);

x = reshape(X',[size(X,1)*size(X,2), 1]);
y = reshape(Y',[size(Y,1)*size(Y,2), 1]);
label = cellfun(@num2str,num2cell(1:Na*Nb),'UniformOutput',0);

offset = a/8;

figure
fig = surf(X,Y,zeros(size(X)));

h = text(x+offset,y+offset, label);
set(h,'Color','b')

end