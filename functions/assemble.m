function A = assemble(elementary, connectivity, N)
% Assembles elementary matrices to form system matrix
% Needs elementary matrix, connectivity and size of the problem

% A = ASSEMBLE(elementary, connectivity,N)

A = sparse(N, N);

% Dof mapping
% As there are three dofs in each node
map = (connectivity-1)*3 + 1;

% For each element
for t=1:size(map,1)
   
    % Positions 
    % pos = [a a+1 ... a+(dof/node-1) b ...]
    
    pos = [map(t,1) map(t,1)+1 map(t,1)+2 map(t,2) map(t,2)+1 map(t,2)+2 ...
           map(t,3) map(t,3)+1 map(t,3)+2 map(t,4) map(t,4)+1 map(t,4)+2]; 
    
    % Positions of elements of elementary matrix in complete matrix
    
    %[(a,a)    (a, a+1)   | (a,b)    (a, b+1);
    % (a+1, a) (a+1, a+1) | (a+1, b) (a+1, b+1);
    % ------------------------------------------
    % (b,a)    (b, a+1)   | (b,b)    (b, b+1);
    % (b+1, a) (b+1, a+1) | (b+1,b)  (b+1, b+1)]
    
    % row of elements: i = a a a a a+1 a+1 a+1 a+1 b b b b b+1 b+1 b+1 b+1
    % column: j = a a+1 b b+1 a a+1 b b+1 a a+1 b b+1 a a+1 b b+1
    
    % If a row of pos is taken and repeated lenght(elementary) times:
    % a a+1 a+2 ... a+gdl/dof-1 b b+1 ... b+gdl/dof-1 c c+1 ...
    % a a+1 a+2 ... a+gdl/dof-1 b b+1 ... b+gdl/dof-1 c c+1 ...
    % a a+1 a+2 ... a+gdl/dof-1 b b+1 ... b+gdl/dof-1 c c+1 ...
    % a a+1 a+2 ... a+gdl/dof-1 b b+1 ... b+gdl/dof-1 c c+1 ...
    % ...
    
    % making a vector with the columns gives i
    % making a vector with the rows gives j
    
    pos1 = reshape(repmat(pos,length(elementary),1), [1 length(elementary)^2]);
    pos2 = reshape(repmat(pos,1,length(elementary)), [1 length(elementary)^2]);
    
    A = A + sparse(pos1, pos2, elementary(:), N, N);
end

end