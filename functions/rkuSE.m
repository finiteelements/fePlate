function [omega, eta] = rkuSE(K, M, E1, E2, E3, nu, rhoEq, Dzero, sizes, modeN, tol)
% Computes natural frequencies and modal strain energy iteratively for a
% dynamic system with a frequency dependent stiffness matrix using RKU
% method.
% 
% [omega, eta] = RKU(K, M, E1, E2, E3, nu, rhoEq, Dzero, heighs, modeN, tol)
%
% Input:
% - K, M: system matrices
% - E1, E2, E3, nu: constants
% - Dzero: value un D at zero frequency
% - sizes: [h1 h2 h3] thickness of layers 
% - modeN: number of modes to compute
% - tol: tolerance
%
% 1: host, 2: visco, 3: constrained

H1 = sizes(1);
H2 = sizes(2);
H3 = sizes(3);

G2 = E2/(2*(1+nu));

% Bending stiffnesses
D1 = E1*H1^3/(12*(1-nu));
D3 = E3*H3^3/(12*(1-nu));

% Eigenvalues when D(0)
[~, lambda] = eigs(K,M,modeN,'smallestabs');
omega = real(sqrt(diag(lambda)));
omega = omega(omega > 1); % For free case

% Constants
H31 = (H1+H3)/2 + H2;
S = 1/(E1*H1) + 1/(E3*H3);
Y = 12*H31^2/(S*(E1*H1^3 + E3*H3^3));

% Initialisation
eta = zeros(modeN,1);

for r = 1:length(omega)

    % Initialisation
    deltaOmega = tol*10;
    k = sqrt(omega(r))*((rhoEq*(H1+H2+H3))/Dzero)^0.25; % Bending wave number

        while deltaOmega > tol
            
            X = G2*S/(k^2*H2);
            Deq = (D1+D3)*(1+X*Y/(1+X));
            
            % Check if Deq is symbolic
            if isa(Deq,'sym')
                [phi,lambdaNew] = eigs(K*double(Deq(omega(r))/Dzero), M,modeN,'smallestabs'); 
                k = sqrt(omega(r))*((rhoEq*(H1+H2+H3))/double(Deq(omega(r))))^0.25;
                % Modal strain energy
                eta(r) = (phi(:,r)'*imag(K*double(Deq(omega(r))/Dzero))*phi(:,r))/(phi(:,r)'*real(K*double(Deq(omega(r))/Dzero))*phi(:,r));
            
            else
                [phi,lambdaNew] = eigs(K*double(Deq/Dzero), M,modeN,'smallestabs');
                k = sqrt(omega(r))*((rhoEq*(H1+H2+H3))/Deq)^0.25;
                % Modal strain energy
                eta(r) = (phi(:,r)'*imag(K*double(Deq/Dzero))*phi(:,r))/(phi(:,r)'*real(K*double(Deq/Dzero))*phi(:,r));
 
            end
            deltaOmega = abs(real(sqrt(lambdaNew(r,r)))-omega(r));
            % Update
            omega(r) = real(sqrt(lambdaNew(r,r))); 
        end
    
end