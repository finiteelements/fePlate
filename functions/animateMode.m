function animateMode(U,coordinates,varargin)
% Animate mode
%
% ANIMATEMODE(u, coordinates)
% 
% where:
% - U: mode vector with shape of the plate
% - coordinates: coordinates of points [x,y]
%
% ANIMATEMODE(u, coordinates, filename)
%
% if a filename is given as a third input, the animation is saved there

x = coordinates{1};
y = coordinates{2};

[X,Y] = meshgrid(x,y);

figure
for t = 0:0.1:10*pi
    
    surf(X,Y, real(U*exp(1j*t)));
    axis([min(x) max(x) min(y) max(y) -max(max(abs(U)))*2 max(max(abs(U)))*2])
    axis manual
    drawnow
    
    if nargin == 3    
        frame = getframe(gcf);
        img =  frame2im(frame);
        [img,cmap] = rgb2ind(img,256);
        if t == 0
            imwrite(img,cmap,varargin{:},'gif','LoopCount',Inf,'DelayTime',0.1);
        else
            imwrite(img,cmap,varargin{:},'gif','WriteMode','append','DelayTime',0.1);
        end
    end
end