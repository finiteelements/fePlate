function omega = theoreticalFrec(a,b,data,BC)
% Computes theoretical natural frequencies of a plate given its size,
% material data and boundary conditions
% 
% B = THEORETICALFREC(a,b,data,BC)
%
% Inputs:
% - a, b: size of the plate
% - data = [D rho t], where:
%   - D: plate bending modulus 
%   - rho: density
%   - t: width
% - BC: number that defines boundary conditions
%   - 1: simply supported in all edges
%   - 2: clamped in all edges
%   - 3: free

% Extract data
D   = data(1);
rho = data(2);
nu  = data(3);
t   = data(4);

% Initialise
w = 1;

switch BC
    
    case 1
        
        m = 1:10;
        n = 1:10;

        beta1 = m*pi/a;
        beta2 = n*pi/b;

        omega = zeros(length(beta1)*length(beta2),1);

        for i = 1:length(beta1)

            for j = 1:length(beta2) 
                omega(w) = sqrt(D/(rho*t))*(beta1(i)^2+beta2(j)^2); 
                w = w + 1;
            end

        end
        
    case 2
        
        G = [1.506 2.5 3.5];
        J = [1.248 4.658 10.02];
        
        omega = zeros(length(G)^2,1);
        
        for i = 1:length(G)

            for j = 1:length(G) 
                lambda = pi^2*sqrt(G(i)^4+(a/b)^4*G(j)^4+2*(a/b)^2*J(i)*J(j));
                omega(w) = lambda/a^2*sqrt(D/(rho*t));
                w = w + 1;
            end

        end
    
    case 3
        
        G = [0 0 1.506 3];
        J = [0 1.216 5.017];
        H = [0 1.248 1.506];
        
        omega = zeros(length(J)^2,1);
        
        for i = 1:length(J)

            for j = 1:length(J) 
                lambda = pi^2*sqrt(G(i)^4+(a/b)^4*G(j)^4+2*(a/b)^2*(J(i)*J(j)+2*nu*(H(i)*H(j)-J(i)*J(j))));
                omega(w) = lambda/a^2*sqrt(D/(rho*t));
                w = w + 1;
            end

        end
        
    otherwise
        
        omega = NaN;
        
end

omega = sort(omega);