function [phi, lambda] = iterativeEigsEta(K, M, D, modeN, tol)
% Computes eigenpairs iteratively for a dynamic system with a frequency 
% dependent stiffness matrix.
% 
% [phi, lambda] = ITERATIVEEIGSETA(K, M, D, modeN,tol)
%
% Input:
% - K, M: system matrices
% - D: frequency dependent equivalent modulus, symbolic function
% - modeN: number of modes to compute
% - tol: tolerance
% 
% Change stop criteria of ITERATIVEEIGS to eta

[mode, lambda] = eigs(K,M,modeN,'smallestabs');
omega = real(sqrt(diag(lambda)));

phi = zeros(length(K), modeN);
lambda = zeros(modeN,1);
eta = zeros(modeN,1);

for r = 1:modeN
    
    % Initialisation
    deltaEta = tol*10;

    while deltaEta > tol
        
        [mode,lambdaNew] = eigs(K*double(D(omega(r))/D(0)), M,modeN,'smallestabs');
        etaNew = imag(lambdaNew(r,r))/real(lambdaNew(r,r));
        deltaEta = abs((etaNew - eta(r))/etaNew)*100;
        lambda(r) = lambdaNew(r,r);
        omega(r) = real(sqrt(lambda(r)));
        eta(r) = etaNew;
        
    end
    phi(:,r)  = mode(:,r); 
    
end