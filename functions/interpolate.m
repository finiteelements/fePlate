function N = interpolate(L1, L2)
% Computes interpolation functions for thin plate given the size of the
% element
% N = INTERPOLATE(L1, L2)

syms x y

% Terms and coefficients
term = [1 x y x^2 x*y y^2 x^3 x^2*y x*y^2 y^3 x^3*y x*y^3];
var = length(term); % number of variables
coef = sym('a',[1,var]); % creates a1, a2, ...

% Vertical displacement
% w = a1+a2*x+a3*y+a4*x^2+a5*x*y+a6*y^2+a7*x^3+a8*x^2*y+a9*x*y^2+a10*y^3+a11*x^3*y+a12*x*y^3;
w(x,y) = coef*term.';

% Rotations
thetaX(x,y) = diff(w,y);
thetaY(x,y) = -diff(w,x);

nodes = [-1 -1; 1 -1; 1 1; -1 1]';

X = nodes(1,:);
Y = nodes(2,:);

% Sustitución en los nodos
a = [w(X,Y) thetaX(X,Y) thetaY(X,Y)];

% Initialisation
A = zeros(var,var);
v = 1:var;

for k = 1:var

[c,t] = coeffs(a(k), coef);
n = ismember(coef,t); 
A(k,nonzeros(n.*v)) = c; % nonzeros(n.*v) finds the position of values of c in matrix A

end

N = term/A;

% The rotations have to be multiplied by the lengths of the element 
N(5:8) = L2/2*N(5:8);
N(9:12) = L1/2*N(9:12); 

end