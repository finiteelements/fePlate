function [phi, lambda] = nelson(K, M, D, modeN, tol)
% Computes eigenpairs iteratively for a dynamic system with a frequency 
% dependent stiffness matrix using Nelson method.
% 
% [phi, lambda] = NELSON(K, M, D, modeN,tol)
%
% Input:
% - K, M: system matrices
% - D: frequency dependent equivalent modulus, symbolic function
% - modeN: number of modes to compute
% - tol: tolerance

[modes, lambda] = eigs(K,M,modeN,'smallestabs');

lambda = diag(lambda);
omega = real(sqrt(lambda));

% Initialisation
phi = zeros(length(K), modeN);

for r = 1:modeN
    
    deltaOmega = tol*10;
    mode = modes(:,r);
    [~, pos] = max(mode); 
    
    while deltaOmega > tol
        
        Kw = K*double(D(omega(r))/D(0));
        deltaK = K*(double(D(omega(r))/D(0))-1);
        deltaLambda = mode'*deltaK*mode;
        hr = -(-deltaLambda*M + deltaK)*mode;
        hr(pos) = 0;
        Fr = -lambda(r)*M + Kw;
        Fr(pos,:) = zeros(1,length(Fr));
        Fr(:,pos) = zeros(length(Fr),1);
        Fr(pos,pos) = 1;
        xr = Fr\hr;
        cr = -mode'*M*xr;
        deltaMode = xr + cr*mode;
        mode = mode + deltaMode;

        % Normalisation
        mode = mode/(sqrt(mode'*M*mode));

        lambda(r) = mode'*Kw*mode;
        deltaOmega = abs(real(sqrt(lambda(r)))-omega(r));
        omega = real(sqrt(lambda));
    end
    phi(:,r) = mode; 

end