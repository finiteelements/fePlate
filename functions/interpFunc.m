function [M_N, K_N] = interpFunc(a,b,nu)

% Given the length of the element produces interpolation functions for
% the creation of element matrices Me and Ke 
%
% [M_N, K_N] = INTERPFUNCT(a,b,nu)
%
% where:
% - a, b: measures of both sizes of element
% - M_N, K_N: symbolic element matrices for integration
% - nu: Poisson's ratio

% Interpolation functions are defined elementwise in the domain [-1 1; -1 1]

syms xi eta x y  

N1 = -1/8*(eta-1)*(xi-1)*(xi^2+xi+eta^2+eta-2);
N2 =  1/8*(eta-1)*(xi+1)*(xi^2-xi+eta^2+eta-2);
N3 = -1/8*(eta+1)*(xi+1)*(xi^2-xi+eta^2-eta-2);
N4 =  1/8*(eta+1)*(xi-1)*(xi^2+xi+eta^2-eta-2);

N5 = -b/16*(eta+1)*(eta-1)^2*(xi-1);
N6 =  b/16*(eta+1)*(eta-1)^2*(xi+1);
N7 =  b/16*(eta-1)*(eta+1)^2*(xi+1);
N8 = -b/16*(eta-1)*(eta+1)^2*(xi-1);

N9  =  a/16*(xi+1)*(xi-1)^2*(eta-1);
N10 =  a/16*(xi-1)*(xi+1)^2*(eta-1);
N11 = -a/16*(xi-1)*(xi+1)^2*(eta+1);
N12 = -a/16*(xi+1)*(xi-1)^2*(eta+1);

N = [N1 N5 N9 N2 N6 N10 N3 N7 N11 N4 N8 N12];

% Derivatives

Nxx = diff(N,xi,2);
Nyy = diff(N,eta,2);
Nxy = diff(N,xi,eta);

% Chain rule
xi  = 2*x/a - 1; 
eta = 2*y/b - 1;

Nxx = Nxx*jacobian(xi,x)^2;
Nyy = Nyy*jacobian(eta,y)^2;
Nxy = Nxy*jacobian(xi,x)*jacobian(eta,y);

B = [Nxx; Nyy; 2*Nxy]; % 3x12

D = [1 nu 0;nu 1 0;0 0 (1-nu)/2];

% Stiffness matrix
K_N = B.'*D*B;

% Mass matrix
M_N = N.'*N;

end