function F = assembleForce(a, b, values, connect, N)
% Assembles force vector from computed elementary force vector. Needs shape
% functions, lenght of element a b for integration, values in
% [startNode,dof,load] triplets and size of the problem.
%
% F = ASSEMBLEFORCE(a, b, values, connect, N)

syms xi eta x y

N1 = -1/8*(eta-1)*(xi-1)*(xi^2+xi+eta^2+eta-2);
N2 =  1/8*(eta-1)*(xi+1)*(xi^2-xi+eta^2+eta-2);
N3 = -1/8*(eta+1)*(xi+1)*(xi^2-xi+eta^2-eta-2);
N4 =  1/8*(eta+1)*(xi-1)*(xi^2+xi+eta^2-eta-2);

N5 = -b/16*(eta+1)*(eta-1)^2*(xi-1);
N6 =  b/16*(eta+1)*(eta-1)^2*(xi+1);
N7 =  b/16*(eta-1)*(eta+1)^2*(xi+1);
N8 = -b/16*(eta-1)*(eta+1)^2*(xi-1);

N9  =  a/16*(xi+1)*(xi-1)^2*(eta-1);
N10 =  a/16*(xi-1)*(xi+1)^2*(eta-1);
N11 = -a/16*(xi-1)*(xi+1)^2*(eta+1);
N12 = -a/16*(xi+1)*(xi-1)^2*(eta+1);

Ni = [N1 N5 N9 N2 N6 N10 N3 N7 N11 N4 N8 N12];

F = sparse(N, 1);

node = values(:,1);
dof  = values(:,2);
load = values(:,3);

% Dof mapping
map = (node-1)*3 + dof;
mapConnect = (connect-1)*3 + 1; % map connectivity

% For each element
for t=1:size(map,1)
    
    % Create elementary matrix
    Px = subs(load(t),[x y],[(xi+1)*a/2 (eta+1)*b/2]); % variable change
    F_N = Px*Ni;
    fe = a/2*b/2*gaussIntegrate(F_N, 2)';
   
    % Positions 
    c = mapConnect(connect(:,1) == node(t),:); % find nodes of the element
    pos = [map(t)+(1:3)-dof(t) c(2)+(0:2) c(3)+(0:2) c(4)+(0:2)]; % organise submatrices
    
    F = F + sparse(pos, ones(size(pos,1),1), fe(:), N, 1);
end

end