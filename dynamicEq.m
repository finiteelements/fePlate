% Computation of FRF in a point of a plate by FE
% ----------------------------------------------
%
% Dynamic response of a plate with several materials is computed 

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

Na = 11;    % number of nodes in x direction
Nb = 11;    % number of nodes in y direction
dof = 3;    % number of dof/node
N = Na*Nb*dof;

% Response data

steps = 1000;
fMin  = 0; % has to be zero
fMax  = 10000;

f = linspace(fMin, fMax, steps); % rad/s

% Plate size
at = 1;
bt = 1;

% t = 0.01; % thin plate, shear doesn't affect
% t = 0.05; % medium plate
t = 0.1; % thick plate, shear effect

option = 'fld';
% option = 'cld';
% option = 'base';
[E1, E2, E3, nu, H1, H2, H3, rho1, rho2, rho3] = data('layers',option,t);

E2 = double(E2(f));

rhoEq = (rho1*H1 + rho2*H2 + rho3*H3)/t;

G1 = E1/(2*(1+nu));
G2 = E2/(2*(1+nu)); 
G3 = E3/(2*(1+nu));

syms x

% Neutral plane
hn = (E1*H1^2 + E2*H2*(2*H1+H2) + E3*H3*(2*H1+2*H2+H3))./(2*(E1*H1+E2*H2+E3*H3));

lim1 = -hn;
lim2 = H1 - hn;
lim3 = H1 + H2 - hn;
lim4 = H1 + H2 + H3 - hn; 

I1 = zeros(size(f));
I2 = zeros(size(f));
I3 = zeros(size(f));

omega1 = sym(zeros(size(f)));
omega2 = sym(zeros(size(f)));

for r = 1:length(f)
    I1(r) = int(x^2, lim1(r), lim2(r));
    I2(r) = int(x^2, lim2(r), lim3(r));
    I3(r) = int(x^2, lim3(r), lim4(r));
end


A = E1/(1-nu^2);
B = E2/(1-nu^2);
C = E3/(1-nu^2);

Deq = A*I1 + B.*I2 + C*I3;
Beq = E1*I1 + E2.*I2 + E3*I3; 

if strcmp(option,'cld') || strcmp(option,'cldfino')  
    
    % For CLD
    
    for r = 1:length(f)
        omega1(r) = int(x,x,lim1(r));
        omega2 = E1*H1*(hn-H1/2)+ E2.*((hn-H1)^2 - x^2)/2;
        omega3 = int(x,x,lim4);

        intOmega1 = int(omega1(r).^2, x, lim1(r), lim2(r));
        intOmega2 = int(omega2(r).^2, x, lim2(r), lim3(r));
        intOmega3 = int(omega3(r).^2, x, lim3(r), lim4(r));
    end
    
    K1 = (G1*Beq.^2)./(E1^2*intOmega1);
    K2 = (G2.*Beq.^2)./(E2.^2.*intOmega2);
    K3 = (G3.*Beq.^2)./(E3.^2.*intOmega3);

    K1 = double(K1);
    K2 = double(K2);
    K3 = double(K3);

    Keq(f) = 1./(1./K1 + 1./K2 + 1./K3);
else
    
    % For FLD
    
    for r = 1:length(f)
        omega1(r) = int(x,x,lim1(r));
        omega2(r) = int(x,x,lim3(r));

        intOmega1 = int(omega1(r).^2, lim1(r), lim2(r));
        intOmega2 = int(omega2(r).^2, lim2(r), lim3(r));
    end

    K1 = (G1*Beq.^2)./(E1^2*intOmega1);
    K2 = (G2.*Beq.^2)./(E2.^2.*intOmega2);

    K1 = double(K1);
    K2 = double(K2);
    Keq = 1./(1./K1 + 1./K2);

end

% If single material Keq = 5*G*t/6;

% S --> t (quadratic shear) TODO: linear shear 4/3

phi = f.*sqrt(rhoEq*t*Deq)./(2*Keq); 
% B --> D, rhoEq*t --> density per unit area

% B_eq takes into account different material, B_K shear

D = Deq./(sqrt(1+phi.^2)+phi).^2;

% Element size
a = at/(Na-1);
b = bt/(Nb-1); 

plotModel(Na, Nb, at, bt)

% Input data
% ----------

% Boundary conditions

boundary = input('Boundary conditions? \n1: simply supported in all edges \n2: clamped in all edges \n3: free \n4: clampled in one edge \n5: simply supported in all corners \n');

point = input('Point force? (true/false)\n');

% Position of the point load 

if point 
    nodeLoad = input('Position of the point load? \n');
    dofLoad  = input('Dof of the point load? \n');
end

% Response 

nodeResp = input('Position of the response? \n');
dofResp  = input('Dof of the response? \n');

% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(a,b,nu);


% Gaussian quadrature
% -------------------

% a/2 and b/2 to adjust interval from [-1 1; -1 1] to [0 a, 0 b] 

ke = D(1)*a/2*b/2*gaussIntegrate(K_N, 3);
me = rhoEq*t*a/2*b/2*gaussIntegrate(M_N, 3);

% Connectivity
% -------------
% The rows represent the nodes the element connects

% 13 --- 14 --- 15 --- 16
% |       |      |      | 
% |       |      |      |
% 9  --- 10 --- 11 --- 12
% |       |      |      | 
% |       |      |      |
% 5  ---  6 ---  7 ---  8
% |       |      |      | 
% |       |      |      |
% 1  ---  2 ---  3 ---  4

% Connectivity rows should start by nodes [1 2 3 5 6 7 9 10 11]
% the last number = n� of elements + n� of multiples of Na = 
% = (Na-1)*(Nb-1) + (Nb-1)
% that makes 1:12. The multiples of Na have to be removed to retain only 
% the needed nodes

Ne = (Na-1)*(Nb-1); % number of elements
nodes = 1:(Ne+(Nb-1));
nodes = nodes(mod(nodes,Na)~=0)';

connect = [nodes nodes+1 nodes+Na+1 nodes+Na];

K = assemble(ke, connect, dof*Na*Nb);
M = assemble(me, connect, dof*Na*Nb);

% Load
% -----

if point 

    % Point load
    P = 1;
    values = [nodeLoad dofLoad P]; % [node, dof, value]

    F = assemblePointF(values, N);

else
    
    nodeLoad = 0;
    % Distributed load in all elements
    values = [nodes ones(size(nodes)) ones(size(nodes))]; % [startNode, dof, value]

    F = assembleForce(a, b, values, connect, N);

end

% Boundary conditions
% --------------------

% Case 1: simply supported in all edges
% Case 2: clamped in all edges
% Case 3: free
% Case 4: clampled in one edge
% Case 5: simply supported in all corners

% Boundaries
low   = 1:Na;
up    = (Nb*Na-Na+1):Nb*Na;
right = 1:Na:(Nb*Na-Na+1);
left  = Na:Na:Nb*Na;

% Corners
lowLeft  = intersect(low,left);
lowRight = intersect(low,right);
upLeft   = intersect(up,left);
upRight  = intersect(up,right);

switch boundary
    
    case 1
    % Simply supported in all ends
    border = [low up right left];
    border = unique(border);
    
    node = border';
    dof = ones(length(border),1);
    value = zeros(length(border),1);
    
    BC = [node dof value];
    
    case 2
    % Clamped in all ends
    border = [low up right left];
    border = unique(border);
    
    node = reshape(repmat(border,3,1), [length(border)*3 1]);
    dof = reshape(repmat([1 2 3],1,length(border)), [length(border)*3 1]);
    value = zeros(length(border)*3,1);
    
    BC = [node dof value];
    
    case 3
    % Free
    BC = [];
    
    case 4
    % Clamped in one end
    border = low;
    
    node = reshape(repmat(border,3,1), [length(border)*3 1]);
    dof = reshape(repmat([1 2 3],1,length(border)), [length(border)*3 1]);
    value = zeros(length(border)*3,1);
    
    BC = [node dof value];
    
    case 5
    % Simply supported in all corners
    border = [lowLeft lowRight upRight upLeft];
    
    node = border';
    dof = ones(length(border),1);
    value = zeros(length(border),1);
    
    BC = [node dof value];
  
    
end

M_BC = applyBC(M, BC);
K_BC = applyBC(K, BC);
F_BC = applyBC(F,BC);


% Response
% --------

mapResp = (nodeResp-1)*3 + dofResp;

% Unit vector with 1 where the response is computed
e = sparse(mapResp,1,1,N,1);
e = applyBC(e, BC);

resp = zeros(steps,1);

x = zeros(steps,1);
x_oberst = zeros(steps,1);

% Load variation in frequency

% Impulse load
P = ones(steps,1);

% Compute response in point 

for t = 1:steps
   
    s = 1i*f(t);
    Z = M_BC*s^2 + K_BC*D(t)/D(1);
    Z_oberst = M_BC*s^2 + K_BC*Deq(t)/D(1);
    
    h = Z\e; % only one column of H needed
    h_oberst = Z_oberst\e; % only one column of H needed
    
    x(t) = h'*F_BC*P(t);
    x_oberst(t) = h_oberst'*F_BC*P(t);
    
end

figure(2)

subplot(211), semilogy(f,abs(x),f,abs(x_oberst)) 
title({'FRF', ['Input node: ' num2str(nodeLoad)], [' Output node: ' num2str(nodeResp)]})
xlabel('\omega [rad/s]'), ylabel('Amplitude [m]')
legend('New model', 'Oberst')

subplot(212), plot(f,angle(x),f,abs(x_oberst))
xlabel('\omega [rad/s]'), ylabel('Angle [rad]')

%%

% Response for every point
% ------------------------

I = eye(length(Z),'like',Z);

x = zeros(length(Z),steps);
x_oberst = zeros(length(Z),steps);

for t = 1:steps
   
    s = 1i*f(t);
    Z = M_BC*s^2 + K_BC*double(D(t)/D(1));
    Z_oberst = M_BC*s^2 + K_BC*double(Deq(t)/D(1));
    
    H = Z\I;
    H_oberst = Z_oberst\I;
    
    x(:,t) = H*F_BC*P(t);
    x_oberst(:,t) = H_oberst*F_BC*P(t);
    
end

% Assemble x
x = assembleBC(x, BC);
x_oberst = assembleBC(x_oberst, BC);

% RMS response of all points
x = x(1:3:end,:); % each column is the response at a certain f
rms = mean(x.^2).^0.5;

x_oberst = x_oberst(1:3:end,:); % each column is the response at a certain f
rms_oberst = mean(x_oberst.^2).^0.5;

figure(3)
semilogy(f,rms, f,rms_oberst)
title('RMS ')
xlabel('\omega [rad/s]'), ylabel('Amplitude [m]')
legend('New model', 'Oberst')