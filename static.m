% Static response of plate using FE
% ---------------------------------

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

Na = 50;    % number of nodes in x direction
Nb = 50;    % number of nodes in y direction
dof = 3;    % number of dof/node

N = dof*Na*Nb; % size of the problem

% Material and section data

% Plate size
at = 1;
bt = 1;
t = 0.1; % thick plate, shear effect

% Material and section data
E = 2e11;
nu = 0.3;

D = E*t^3/(12*(1-nu^2));

% Element size
a = at/(Na-1);
b = bt/(Nb-1); 

plotModel(Na, Nb, at, bt)


% Interpolation functions
% -----------------------

[~, K_N] = interpFunc(a,b,nu);


% Gaussian quadrature
% -------------------

% a/2 and b/2 to adjust interval from [-1 1; -1 1] to [0 a, 0 b] 

ke = D*a/2*b/2*gaussIntegrate(K_N, 3);

% Connectivity
% -------------

Ne = Na*Nb; % number of elements
nodes = 1:((Na-1)*(Nb-1)+(Nb-1));
nodes = nodes(mod(nodes,Na)~=0)'; % first node of every element

connect = [nodes nodes+1 nodes+Na+1 nodes+Na];

K = assemble(ke, connect, dof*Na*Nb);

% Load
% -----

point = false;

if point 

    % Point load
    P = -1;
    values = [15 1 P]; % [node, dof, value]

    F = assemblePointF(values, N);

else

    % Distributed load in all elements
    values = [nodes ones(size(nodes)) ones(size(nodes))]; % [startNode, dof, value]

    F = assembleForce(a, b, values, connect, N);

end

% Boundary conditions
% --------------------

% Case 1: simply supported in all edges
% Case 2: clamped in all edges
% Case 3: free
% Case 4: clampled in one edge
% Case 5: simply supported in all corners

boundary = 2;

% Boundaries
low   = 1:Na;
up    = (Nb*Na-Na+1):Nb*Na;
right = 1:Na:(Nb*Na-Na+1);
left  = Na:Na:Nb*Na;

% Corners
lowLeft  = intersect(low,left);
lowRight = intersect(low,right);
upLeft   = intersect(up,left);
upRight  = intersect(up,right);

switch boundary
    
    case 1
    % Simply supported in all ends
    border = [low up right left];
    border = unique(border);
    
    node = border';
    dof = ones(length(border),1);
    value = zeros(length(border),1);
    
    BC = [node dof value];
    
    case 2
    % Clamped in all ends
    border = [low up right left];
    border = unique(border);
    
    node = reshape(repmat(border,3,1), [length(border)*3 1]);
    dof = reshape(repmat([1 2 3],1,length(border)), [length(border)*3 1]);
    value = zeros(length(border)*3,1);
    
    BC = [node dof value];
    
    case 3
    % Free
    BC = [];
    
    case 4
    % Clamped in one end
    border = low;
    
    node = reshape(repmat(border,3,1), [length(border)*3 1]);
    dof = reshape(repmat([1 2 3],1,length(border)), [length(border)*3 1]);
    value = zeros(length(border)*3,1);
    
    BC = [node dof value];
    
    case 5
    % Simply supported in all corners
    border = [lowLeft lowRight upRight upLeft];
    
    node = border';
    dof = ones(length(border),1);
    value = zeros(length(border),1);
    
    BC = [node dof value];
  
    
end

K_BC = applyBC(K, BC);
F_BC = applyBC(F,BC);

uStatic = K_BC\F_BC;

% Assemble BC
uStatic = assembleBC(uStatic, BC);

% Take only displacements
uStatic = uStatic(1:3:end);

U = reshape(uStatic,Na,Nb);

% Coordinates of nodes
x = 0:a:at; 
y = 0:b:bt;

[X,Y] = meshgrid(x,y);

figure
fig = surf(X,Y,U');

% Compute RMS value
rms = mean(abs(uStatic).^2).^0.5; % mean is done in every column

% RESULTS: for 50x50 nodes, clamped and unit pressure
% Max value: 6.9044e-11
% RMS value: 2.8939e-11