% Symbolic computations for FLD plate for paper

syms H1 H2 E1 E2 rho1 rho2 rho3 t nu1 nu2 f

rhoEq = (rho1*H1 + rho2*H2)/t;

G1 = E1/(2*(1+nu1));
G2 = E2/(2*(1+nu2)); 

syms x f

% Neutral plane
% hn = (E1*H1^2 + E2*H2*(2*H1+H2))/(2*(E1*H1+E2*H2));
syms hn

lim1 = -hn;
lim2 = H1 - hn;
lim3 = H1 + H2 - hn;

I1 = int(x^2, lim1, lim2);
I2 = int(x^2, lim2, lim3);

A = E1/(1-nu1^2);
B = E2/(1-nu2^2);

Deq = simplify(A*I1 + B*I2);
Beq = simplify(E1*I1 + E2*I2);

omega1 = int(E1*x,x,lim1);
K1 = (G1*Beq^2)/int(omega1^2, lim1, lim2);
K1 = simplify(K1);

omega2 = int(x,x,lim3);
K2 = (G2*Beq^2)/(E2^2*int(omega2^2, x, lim2, lim3));
Keq = 1/(1/K1 + 1/K2);

phi = f*sqrt(rhoEq*t*Deq)/(2*Keq);

Dw = Deq/(sqrt(1+phi^2)+phi)^2;

% Single material
% -------------

syms E nu

E1 = E;
E2 = E1;
nu1 = nu;
nu2 = nu1;
G = E/(2*(1+nu));
H1 = t/2;
H2 = H1;
hn = subs(hn);
D = subs(Deq);
K = simplify(subs(K1));
Kt = simplify(subs(Keq)); % --> 5*t*G/6

% Same result with H2 = H1 = t/2 and H2 = 0 and H1 = t