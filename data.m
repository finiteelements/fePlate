function varargout = data(varargin)
% Material and section data for the computation of modes
%
% For steel:
% [E, nu, rho] = DATA('steel');
%
% For a layered plate: 
% [E1, E2, E3, nu, H1, H2, H3, rho1, rho2, rho3] = DATA('layers',option,t);
%
% where:
% 1: base material
% 2: viscoelastic material
% 3: base material (for CLD)
% 
% Possible values of option:
% - cld: three layers
% - fld: two layers
% - base: only base material, for testing
% - visco: only viscoelastic material, for testing 
%
% For comparing the equivalent element to Ansys results:
% [E1, E2, E3, nu, H1, H2, H3, rho1, rho2, rho3] = DATA('ansys', option,t);
%
% Possible values of option:
% - cld: three layers
% - fld: two layers

option = varargin{1};

switch option
    
    case 'steel'
        % For testing
        E = 210E9;
        rho = 7900;
        nu = 0.3;
        
        varargout{1} = E;
        varargout{2} = nu;
        varargout{3} = rho;
        
    case 'layers'
        
        option = varargin{2};
        t = varargin{3};
        
        % Material data
        rho1 = 7782; 
        rho2 = 1423;
        rho3 = rho1;

        E1 = 176.2E9;
        nu = 0.3;

        % Fractional damping
        E_0   = 0.353E9;
        E_inf = 3.462E9;
        tau   = 314.9E-6;
        alpha = 0.873;
        
        syms f
        
        E2(f) = (E_0 + E_inf*(1i*tau*f)^alpha)/(1+(1i*tau*f)^alpha);
        
        switch option
            
            case 'cld'
            H1 = t/4;     % base material
            H3 = t/2;     % base material
            H2 = t-H1-H3;  % viscoelastic material
            E3 = E1;
            
            case 'cldfino'
            H1 = t/20;     % base material
            H3 = t/20;     % base material
            H2 = t-H1-H3;  % viscoelastic material
            E3 = E1;

            case 'fld'
            H1 = t/2;     % base material
            H3 = 0;       % base material
            H2 = t-H1-H3; % viscoelastic material
            E3 = 0;

            case 'base'
            % Only base material
            H1 = t; % only base material
            H3 = 0;
            H2 = t-H1-H3; % viscoelastic material
            E3 = E1;

            case 'visco'
            % Only viscoelastic
            H1 = 0; % only viscoelatic material
            H3 = 0;
            H2 = t-H1-H3; % viscoelastic material
            E3 = E1;
        
        end

        varargout{1} = E1;
        varargout{2} = E2;
        varargout{3} = E3;
        
        varargout{4} = nu;
        
        varargout{5} = H1;
        varargout{6} = H2;
        varargout{7} = H3;
        
        varargout{8} = rho1;
        varargout{9} = rho2;
        varargout{10} = rho3;
        
    case 'ansys'
        
        E1 = 176.2E9;
        E2 = 0.353E9;
        E3 = E1;
        
        nu = 0.3;
        
        rho1 = 7782; 
        rho2 = 1423;
        rho3 = rho1;
        
        option = varargin{2};
        t = varargin{3};
        
        switch option
        
            case 'cld'
            H1 = t/4;     % base material
            H3 = t/2;     % base material
            H2 = t-H1-H3;  % viscoelastic material
            E3 = E1;
            
            case 'cldfino'
            H1 = t/20;     % base material
            H3 = t/20;     % base material
            H2 = t-H1-H3;  % viscoelastic material
            E3 = E1;
        
            case 'fld'
            H1 = t/2;     % base material
            H3 = 0;       % base material
            H2 = t-H1-H3; % viscoelastic material           

        end           
        
        varargout{1} = E1;
        varargout{2} = E2;
        varargout{3} = E3;
        
        varargout{4} = nu;
        
        varargout{5} = H1;
        varargout{6} = H2;
        varargout{7} = H3;
        
        varargout{8} = rho1;
        varargout{9} = rho2;
        varargout{10} = rho3;

end